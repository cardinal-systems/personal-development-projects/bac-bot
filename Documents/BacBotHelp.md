**Bac Bot Help Guide**
Below are a list of tricks (or commands) that Bac Bot can do, as well as when to run them:

```js
bacbot help
"Gets Bac Bot to fetch this list of useful commands."

bacbot help tricks
"Gets Bac Bot to fetch a list of trick commands."

bacbot help voice
"Gets Bac Bot to fetch a list of voice commands."

bacbot speak
"Bac Bot speaks to you."

bacbot mlem
"Licks your yogurt pot clean."

bacbot sing
"Bac Bot sings a special song for you."

bacbot meme
"Bac Bot fetches you a random meme that he found pretty funny."

bacbot beans
"Bac Bot Beans.exe"

bacbot friend
"Bac Bot finds you an image of a friend of he knows."

bacbot teach trick {trickCommand} {trickAction}
"You can teach Bac Bot some new tricks so they can perform the most important of tasks."

bacbot fetch {userName}
"Gets Bac Bot to fetch a user through private messaging them that you want to chat with them."

bacbot trick {name}
"Bac Bot will perform any of the tricks you have created for Bac Bot to use."

bacbot roll
"Bac Bot will roll you a single D20 dice."

bacbot roll {amount} {sides}
"Bac Bot will roll you a number of dice with a number of sides and return you the result"
```