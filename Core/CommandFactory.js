const Constants = require("../Core/Constants.json");

const Logger = require("./Logger");

const AudioDataAccess = require("../DataAccess/AudioDataAccess");
const CommonDataAccess = require("../DataAccess/CommonDataAccess");
const DiceDataAccess = require("../DataAccess/DiceDataAccess");
const FileDataAccess = require("../DataAccess/FileDataAccess");
const MemeDataAccess = require("../DataAccess/MemeDataAccess");
const TricksDataAccess = require("../DataAccess/TricksDataAccess");

class CommandFactory {
    constructor(bot, message)
    {
        this.message = message;

        this.logger = new Logger("CommandFactory");

        this.file = new FileDataAccess();
        this.meme = new MemeDataAccess();
        this.dice = new DiceDataAccess();
        this.audio = new AudioDataAccess(bot, {channel: message.channel, member: message.member})
        this.common = new CommonDataAccess(bot, {channel: message.channel, member: message.member});
        this.tricks = new TricksDataAccess(bot, {channel: message.channel, user: message.author});

        try {
            this.arguments = message.content.slice(7).match(/(?:[^\s"]+|"[^"]*")+/g);
            this.command = this.arguments[0];
            this.commandParameters = this.arguments.slice(1);
        } catch {
            this.logger.Error(`Error processing argements from message - ${message.content}`);
        }
    }

    async ProcessCommand()
    {
        switch (this.command.toLowerCase()) {
            case "help":
                switch(this.commandParameters[0]){
                    case "tricks":
                        this.tricks.GetTricks();
                        try {
                            this.common.SendMessage(this.file.ReadFile(Constants.HelpTrickFile));
                        }
                        catch (err) {
                            this.common.SendMessage("*Bac Bot returned looking sad...*");
                        }
                        break;
                    case "voice":
                        try {
                            this.common.SendMessage(this.file.ReadFile(Constants.HelpVoiceFile));
                        }
                        catch (err) {
                            this.common.SendMessage("*Bac Bot returned looking sad...*");
                        }
                        break;
                    default:
                        try {
                            this.common.SendMessage(this.file.ReadFile(Constants.HelpFile));
                        }
                        catch (err) {
                            this.common.SendMessage("*Bac Bot returned looking sad...*");
                        }
                        break;
                }
                break;

            case "trick":
                this.tricks.PerformTrick(this.commandParameters[0]);
                break;

            case "teach":
                switch(this.commandParameters[0]){
                    case "trick":
                        this.commandParameters.length == 3 ?
                        this.tricks.TeachTrick(this.commandParameters[1], this.commandParameters[2]) :
                        this.ProcessCommandFailure();
                        break;
                    default:
                        this.ProcessCommandFailure();
                        break;
                }
                break;

            case "fetch":
                this.common.SendPrivateMessage(
                    this.commandParameters[0], 
                    `**Bac Bot wants you to follow him to ${this.message.guild.name}**`
                );
                break;

            case "join":
                this.common.JoinVoiceChat();
                break;

            case "leave":
                this.common.LeaveVoiceChat();
                break;

            case "speak":
                this.audio.PlayAudio("./Audio/BacBac/bark.mp3");
                break;
    
            case "mlem":
                this.audio.PlayAudio("./Audio/BacBac/trappy_tounges.mp3");
                break;

            case "sing":
                var songPath = this.file.RandomFile("./Audio/Songs");
                if (songPath) {
                    this.audio.PlayAudio(songPath);
                } else {
                    this.common.SendMessage("*Bac Bot returned looking sad...*");
                }
                break;
            
            case "meme":
                var randomMeme = await this.meme.GetMeme("dogmemes");
                this.common.SendMessage(randomMeme);
                break;

            case "beans":
                var randomMeme = await this.meme.GetMeme("Beans");
                this.common.SendMessage(randomMeme);
                break;

            case "friend":
                var randomPuppy = await this.meme.GetPuppy();
                this.common.SendMessage(randomPuppy);
                break;

            case "roll":
                var rollResult = this.commandParameters.length == 2 ?
                    this.dice.RollDice(this.commandParameters[0], this.commandParameters[1]) :
                    this.dice.RollDice();
                this.common.SendMessage(rollResult)
                break;

            default:
                this.ProcessCommandFailure();
                break;
        }
    }

    ProcessCommandFailure() {
        this.common.SendMessage("***BARK?*** *(What does homan mean?)*");
    }
}

module.exports = CommandFactory