const moment = require("moment");

class Logger {
    constructor(serviceLocation) {
        this.serviceLocation = serviceLocation || "BacBot";
    }

    TimeStamp(date) {
        return moment(date).format("DD-MM-YY HH:mm:ss");
    }

    Debug(message) {
        if (process.env.LOGGER == "verbose" || process.env.LOGGER == "debug") {
            console.debug("\x1b[32m%s",`${this.TimeStamp()} - DEBUG - ${this.serviceLocation} - ${message}`);
        }
    }

    Warn(message) {
        if (process.env.LOGGER == "verbose" || process.env.LOGGER == "debug" || process.env.LOGGER == "warn") {
            console.warn("\x1b[33m%s",`${this.TimeStamp()} - WARNING - ${this.serviceLocation} - ${message}`);
        }
    }

    Error(message) {
        console.error("\x1b[31m%s",`${this.TimeStamp(Date.now())} - ERROR - ${this.serviceLocation} - ${message}`);
    }
}

module.exports = Logger;