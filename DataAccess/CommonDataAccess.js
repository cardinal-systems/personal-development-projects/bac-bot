const Logger = require("../Core/Logger");

class CommonDataAccess {
    constructor(bot, options) {
        this.bot = bot;
        this.options = options ? options : {}
        this.logger = new Logger("CommonDataAccess");
    }

    async JoinVoiceChat()
    {
        if (this.options.member.voice.channel) {
            return await this.options.member.voice.channel.join();
        } else {
            this.SendMessage("**Bac Bot looks confused?**");
        }
    }

    async LeaveVoiceChat()
    {
        if (this.options.member.voice.channel) {
            this.options.member.voice.channel.leave();
        } else {
            this.SendMessage("**Bac Bot looks confused?**");
        }
    }

    SendMessage(message)
    {
        this.options.channel.send(message);
    }

    SendTTSMessage(message)
    {
        this.options.channel.send(message, {
            tts: true
        });
        
    }

    SendPrivateMessage(user, message)
    {
        this.options.channel.guild.members.fetch({ query: user, limit: 1 })
            .then((res) => {
                const userId = res.keys().next().value;
                res.get(userId).user.send(message);
            })
            .catch((err) => {
                this.logger.Error(`Message sent to - ${err}`);
            });
    }
}

module.exports = CommonDataAccess;