const CommonDataAccess = require("./CommonDataAccess");
const FileDataAccess = require("./FileDataAccess");
const Logger = require("../Core/Logger");

const Constants = require("../Core/Constants.json");

class TricksDataAccess {

    constructor(bot, options) {
        this.logger = new Logger("TricksDataAccess");

        this.file = new FileDataAccess();
        this.common = new CommonDataAccess(bot, options);
    }

    GetTricks()
    {
        var tricks = JSON.parse(this.file.ReadFile(Constants.TrickFile));
        var tricksList = "";

        Object.keys(tricks).forEach((trick) => {
            trick.split(" ").length > 1 ?
            tricksList = tricksList.concat(`bacbot trick "${trick}"\n\n`) :
            tricksList = tricksList.concat(`bacbot trick ${trick}\n\n`)
        });

        var helpData = `**Bac Bot Trick Help**\nBelow is a list of trick commands that Bac Bot has learned that you can use:\n\n\`\`\`js\n${tricksList}\`\`\``;
        this.file.WriteFile(Constants.HelpTrickFile, helpData);
    }

    PerformTrick(trick)
    {
        var trick = trick.replace(/"/g, '');

        const tricks = JSON.parse(this.file.ReadFile(Constants.TrickFile));
        const commands = Object.keys(tricks);

        if (commands.includes(trick)) {
            this.common.SendTTSMessage(tricks[trick]);
            return;
        }
        return this.common.SendMessage("**Looks at you puzzled?**");
    }

    TeachTrick(trickCommand, trickAction)
    {
        var trickCommand = trickCommand.replace(/"/g, "");
        var trickAction = trickAction.replace(/"/g, "");

        const tricks = JSON.parse(this.file.ReadFile(Constants.TrickFile));
        const commands = Object.keys(tricks);

        if (!commands.includes(trickCommand) && trickAction != "") {
            tricks[trickCommand] = trickAction;
            this.file.WriteFile(Constants.TrickFile, JSON.stringify(tricks));
            this.common.SendTTSMessage(trickAction);
            return;
        }
        return this.common.SendMessage("**Bac Bot already knows that one**");
    }

}

module.exports = TricksDataAccess;