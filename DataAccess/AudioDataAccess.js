const CommonDataAccess = require("../DataAccess/CommonDataAccess");
const Logger = require("../Core/Logger");

class AudioDataAccess {
    constructor(bot, options) {
        this.bot = bot;
        this.options = options ? options : {}

        this.logger = new Logger("VoiceDataAccess");
        this.common = new CommonDataAccess(bot, options);
    }

    PlayAudio(audioPath)
    {
        this.common.JoinVoiceChat().then((connection) => {
            const dispatcher = connection.play(audioPath);
            dispatcher.on("finish", async () => await this.common.LeaveVoiceChat());
        });
    }
}

module.exports = AudioDataAccess;