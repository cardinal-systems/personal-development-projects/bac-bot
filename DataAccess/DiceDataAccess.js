class DiceDataAccess {
    constructor() {}

    RollDice(numberOfDice, numberOfSides) {
        if (isNaN(numberOfDice) && isNaN(numberOfSides) || numberOfSides < 2) {
            numberOfDice = 1;
            numberOfSides = 20;
        }

        var result = {}
        var rolls = [];

        for (var i = 0; i < numberOfDice; i++) {
            rolls.push(Math.floor(Math.random() * (numberOfSides - 1 + 1) + 1));
        }
        result.rolls = rolls;
        result.sumRolls = result.rolls.reduce((a,b) => a + b, 0);
        return result.sumRolls;
    }
}

module.exports = DiceDataAccess;