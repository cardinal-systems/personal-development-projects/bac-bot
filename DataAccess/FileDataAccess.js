const fs = require("fs");
const Logger = require("../Core/Logger");

class FileDataAccess {
    constructor() { 
        this.logger = new Logger("FileDataAccess");
    }

    ReadFile(filePath) {
        return fs.readFileSync(filePath, "utf8");
    }

    async WriteFile(filePath, data) {
        await fs.writeFileSync(filePath, data);
    }

    RandomFile(folderPath) {
        var fileList = fs.readdirSync(folderPath, (err, files) => {
            if (err) {
                return;
            }
            return files;
        });
        const randomFile = fileList[Math.floor(Math.random() * fileList.length)];
        return folderPath.concat(`/${randomFile}`);
    }
}

module.exports = FileDataAccess;