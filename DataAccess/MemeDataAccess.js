const randomPuppy = require("random-puppy");

class MemeDataAccess {

    constructor () {}

    async GetMeme(memeSubReddit) {
        var result = await randomPuppy(memeSubReddit).then((url) => {
            return url
        });
        return result;
    }

    async GetPuppy() {
        var puppy = await randomPuppy().then((url) => {
            return url
        });
        return puppy;
    }

}

module.exports = MemeDataAccess;